package com.simonas.joris.maptest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by JORIS on 2015.03.07.
 */
public class Vehicle {

    //TODO use LatLng
    public Coordinate Coordinate;
    @SerializedName("Type")
    public int Type;
    @SerializedName("MapIcon")
    public String MapIcon;
    @SerializedName("MapIconSize")
    public int MapIconSize;
    @SerializedName("ZoomLevel")
    public float ZoomLevel;
    @SerializedName("Color")
    public String Color;
    @SerializedName("Angle")
    public float Angle;
    @SerializedName("TooltipTitleText")
    public String TooltipTitleText;
    @SerializedName("TooltipSubtitleText")
    public String TooltipSubtitleText;
    @SerializedName("VehicleId")
    public String VehicleId;
}
