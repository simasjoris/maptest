package com.simonas.joris.maptest.model;
import com.google.gson.annotations.SerializedName;

/**
 * Created by JORIS on 2015.03.07.
 */
public class Coordinate {
    @SerializedName("Lat")
    public float Latitude;
    @SerializedName("Lng")
    public float  Longitude;
}
