package com.simonas.joris.maptest.util;

import android.os.AsyncTask;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * Created by simonassamaitis on 2/25/15.
 */
public class ConnectionTask extends AsyncTask<String, Void, String> {

    private TaskCompletedListener mListener;

    public ConnectionTask(TaskCompletedListener listener) {
        mListener = listener;
    }

    @Override
    protected String doInBackground(String... params) {

        java.net.URL url = null;
        try {
            url = new URL(params[0]);
        } catch (MalformedURLException e) {
            return null;
        }

        String httpMethod = params[1];
        String query = params[2];

        try {

            HttpURLConnection httpURLConnection = null;
            if(httpMethod.equals("GET"))
            {
                httpURLConnection = (HttpURLConnection)url.openConnection();
            }
            else if(httpMethod.equals("POST"))
            {
                httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setDoOutput(true); // Triggers POST.
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                if(query != null) {
                    OutputStream output = httpURLConnection.getOutputStream();
                    output.write(query.getBytes());
                }
            }
            else
            {
                return null;
            }

            int status = httpURLConnection.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    /* Define InputStreams to read from the URLConnection. */
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedInputStream aBufferedInputStream = new BufferedInputStream(inputStream);

                    /* Read bytes to the Buffer until there is nothing more to read(-1) */
                    ByteArrayBuffer aByteArrayBuffer = new ByteArrayBuffer(50);
                    int current = 0;
                    while ((current = aBufferedInputStream.read()) != -1) {
                        aByteArrayBuffer.append((byte)current);
                    }

                    /* Convert the Bytes read to a String. */
                    String result = new String(aByteArrayBuffer.toByteArray());
                    return result;
                default:
                    return String.valueOf(status);
            }
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        mListener.onTaskCompleted(result);

    }

    public interface TaskCompletedListener {
        void onTaskCompleted(String result);
    }

}

