package com.simonas.joris.maptest;

import android.content.Context;

/**
 * Created by JORIS on 2015.03.07.
 */
public interface IMapView {
    public Context getContext();
}
