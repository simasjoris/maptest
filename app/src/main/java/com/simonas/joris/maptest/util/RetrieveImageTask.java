package com.simonas.joris.maptest.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by JORIS on 2015.03.07.
 */
public class RetrieveImageTask extends AsyncTask<String, Void, Bitmap> {

        private TaskCompletedListener mListener;

        public RetrieveImageTask(TaskCompletedListener listener) {
            mListener = listener;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            java.net.URL url = null;
            try {
                url = new URL(params[0]);
            } catch (MalformedURLException e) {
                return null;
            }

            try {
                return BitmapFactory.decodeStream((InputStream) url.getContent());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            mListener.onTaskCompleted(result);

        }

        public interface TaskCompletedListener {
            void onTaskCompleted(Bitmap result);
        }
}
