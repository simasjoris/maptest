package com.simonas.joris.maptest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Handler;
import android.os.SystemClock;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.simonas.joris.maptest.model.Vehicle;
import com.simonas.joris.maptest.util.BitmapUtil;
import com.simonas.joris.maptest.util.RetrieveImageTask;

/**
 * Created by JORIS on 2015.03.07.
 */
public class MapMarker {

    private static final int MARKER_ANIMATION_DURATION = 3000;

    private GoogleMap mMap;

    private Marker mMarker;
    private Vehicle mVehicle;
    private LatLng mPosition;
    private Context mContext;
    public boolean isUpdated = true;
    private boolean isDead = false;
    private Bitmap mVehicleNumberBitmap;

    public MapMarker(Context context, GoogleMap map, LatLng position, Vehicle vehicle){
        mContext = context;
        mMap = map;
        mVehicle = vehicle;
        mPosition = position;
        getMarkerIcon();
    }

    public void animateMarker(final LatLng toPosition, Projection projection) {
        isUpdated = true;
        if(mMarker != null) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            Point startPoint = projection.toScreenLocation(mMarker.getPosition());
            final LatLng startLatLng = projection.fromScreenLocation(startPoint);

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed
                            / MARKER_ANIMATION_DURATION);
                    double lng = t * toPosition.longitude + (1 - t)
                            * startLatLng.longitude;
                    double lat = t * toPosition.latitude + (1 - t)
                            * startLatLng.latitude;
                    mMarker.setPosition(new LatLng(lat, lng));

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    }
                }
            });
        }
    }

    public void remove(){
        if(mMarker != null) {
            mMarker.remove();
        }
        else
        {
            isDead = true;
        }
    }

    private void addMarker(Bitmap icon){
        if(!isDead) {
            mMarker = mMap.addMarker(new MarkerOptions()
                    .position(mPosition).title("Marker")
                    .icon(BitmapDescriptorFactory.fromBitmap(icon)));
        }
    }

    private Bitmap createMarkerIcon(Bitmap topBitmap){
        Bitmap bottomBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_vehicle_bg);
        bottomBitmap = BitmapUtil.changeBitmapColor(bottomBitmap, mVehicle.Color);
        bottomBitmap = BitmapUtil.rotateBitmap(bottomBitmap, mVehicle.Angle);
        return BitmapUtil.combineBitmaps(bottomBitmap, topBitmap);
    }

    public void getMarkerIcon() {
        String URL = "http://cdn.trafi.com/icon.ashx?style=androidv2&size=48&src="+mVehicle.MapIcon
                +"&cl=ffffff";
        new RetrieveImageTask(new RetrieveImageTask.TaskCompletedListener() {
            @Override
            public void onTaskCompleted(Bitmap result) {
                mVehicleNumberBitmap = result;
                Bitmap icon = createMarkerIcon(result);
                addMarker(icon);
            }
        }).execute(URL);
    }

    public Marker getMarker() {
        return mMarker;
    }

    public Vehicle getVehicle() {
        return mVehicle;
    }

    public Bitmap createVehicleNumberIcon() {
        int bitmapSize = (int) BitmapUtil.dipToPixels(mContext, 24);
        Bitmap baseBitmap = BitmapUtil.createBitmapRect(bitmapSize, bitmapSize, mVehicle.Color);
        int roundingPixels = (int) BitmapUtil.dipToPixels(mContext, 2);
        baseBitmap = BitmapUtil.getRoundedCornerBitmap(baseBitmap, roundingPixels);
        return BitmapUtil.combineBitmaps(baseBitmap, mVehicleNumberBitmap);
    }
}
