package com.simonas.joris.maptest;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.melnykov.fab.FloatingActionButton;
import com.simonas.joris.maptest.model.Vehicle;
import com.simonas.joris.maptest.util.BitmapUtil;
import com.simonas.joris.maptest.util.ConnectionTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by JORIS on 2015.03.07.
 */
public class MapPresenter {

    private static final float ZOOM = 14.0f;
    private static final int MARKER_UPDATE_FREQUENCY = 5000;
    private static final int MAX_MARKERS = 10;

    private Timer mTimer;

    private IMapView mMapView;
    private GoogleMap mMap;

    private Projection mProjection;
    private Hashtable<String, MapMarker> mMapMarkers = new Hashtable<String, MapMarker>();

    private boolean isLocationVisible = true;
    private boolean isVehiclesVisible = true;

    public MapPresenter(IMapView mapView){
        mMapView = mapView;
    }
    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    public void setUpMap(GoogleMap map) {
        mMap = map;
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                mProjection = mMap.getProjection();
                getVehicles();
            }
        });
    }

    public void updateMyLocation(Location location){

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        LatLng latLng = new LatLng(latitude, longitude);

        if(mTimer == null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM), new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    mProjection = mMap.getProjection();
                    mTimer = new Timer();
                    mTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            getVehicles();
                        }
                    }, 0, MARKER_UPDATE_FREQUENCY);
                }

                @Override
                public void onCancel() {
                }
            });
        }

    }
    private void getVehicles(){
        if(isVehiclesVisible) {
            LatLngBounds bounds = mProjection.getVisibleRegion().latLngBounds;
            String URL = "http://api-lithuania-dev.trafi.com/api/v2/mapObjects?userlocationId=vilnius&southWestLat=" + bounds.southwest.latitude +
                    "&southWestLng=" + bounds.southwest.longitude +
                    "&northEastLat=" + bounds.northeast.latitude +
                    "&northEastLng=" + bounds.northeast.longitude;
            new ConnectionTask(new ConnectionTask.TaskCompletedListener() {
                @Override
                public void onTaskCompleted(String result) {
                    Type vehicleListType = new TypeToken<List<Vehicle>>() {
                    }.getType();
                    JSONArray jsonArray = null;
                    try {
                        jsonArray = new JSONObject(result).getJSONArray("MapMarkers");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    List<Vehicle> vehicles = new Gson().fromJson(jsonArray.toString(), vehicleListType);
                    updateMapMarkers(vehicles);
                }
            }).execute(URL, "GET", null);
        }
    }

    private void updateMapMarkers(List<Vehicle> vehicles){
        for(Vehicle marker : vehicles){
            if(marker.Type == 2) {
                if(mMapMarkers.containsKey(marker.VehicleId)){
                    mMapMarkers.get(marker.VehicleId).animateMarker(new LatLng(marker.Coordinate.Latitude, marker.Coordinate.Longitude), mProjection);
                }
                else if(mMapMarkers.size() < MAX_MARKERS) {
                    mMapMarkers.put(marker.VehicleId, new MapMarker(mMapView.getContext(), mMap, new LatLng(marker.Coordinate.Latitude, marker.Coordinate.Longitude), marker));
                }
            }
        }
        cleanupMarkers();
    }

    private void cleanupMarkers(){
        Enumeration e = mMapMarkers.keys();
        while (e.hasMoreElements()) {
            String id = (String) e.nextElement();
            MapMarker marker = mMapMarkers.get(id);
            if(marker.isUpdated == false){
                marker.remove();
                mMapMarkers.remove(id);
            }
            else{
                marker.isUpdated = false;
            }
        }
    }

    public void toggleVehicles(FloatingActionButton button) {
        isVehiclesVisible = !isVehiclesVisible;
        Resources res = mMapView.getContext().getResources();
        if(isVehiclesVisible)
        {
            button.setImageDrawable(res.getDrawable(R.drawable.ic_vehicles_on));
            getVehicles();

        }
        else
        {
            button.setImageDrawable(res.getDrawable(R.drawable.ic_vehicles_off));
            cleanupMarkers();
        }

    }

    public void toggleLocation(FloatingActionButton button) {
        isLocationVisible = !isLocationVisible;
        mMap.setMyLocationEnabled(isLocationVisible);
        Resources res = mMapView.getContext().getResources();
        if(isLocationVisible)
        {
            button.setImageDrawable(res.getDrawable(R.drawable.ic_current_location_on));
        }
        else
        {
            button.setImageDrawable(res.getDrawable(R.drawable.ic_current_location_off));
        }
    }

    public void toggleTooltip(Marker selectedMarker, LinearLayout tooltipLayout, ImageView tooltipImage, TextView tooltipText) {
        Enumeration e = mMapMarkers.keys();
        while (e.hasMoreElements()) {
            String id = (String) e.nextElement();
            MapMarker marker = mMapMarkers.get(id);
            if(marker.getMarker() != null && marker.getMarker().equals(selectedMarker)){
                tooltipLayout.animate().translationY(0);
                tooltipText.setText(marker.getVehicle().TooltipTitleText);
                tooltipImage.setImageBitmap(marker.createVehicleNumberIcon());
            }
        }
    }
}
