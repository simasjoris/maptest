package com.simonas.joris.maptest;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.melnykov.fab.FloatingActionButton;
import com.simonas.joris.maptest.util.BitmapUtil;

public class MapsActivity extends FragmentActivity implements IMapView, LocationListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    private static final int LOCATION_UPDATE_FREQ = 20000;
    private MapPresenter mMapPresenter;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private LinearLayout mTooltipLayout;
    private ImageView mTooltipImage;
    private TextView mTooltipText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMapPresenter = new MapPresenter(this);
        setContentView(R.layout.activity_maps);
        mTooltipLayout = (LinearLayout)findViewById(R.id.tooltip);
        mTooltipLayout.setTranslationY(BitmapUtil.dipToPixels(getContext(), -64.0f));
        mTooltipImage = (ImageView)findViewById(R.id.tooltip_icon);
        mTooltipText = (TextView)findViewById(R.id.tooltip_title);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        centerMapOnMyLocation();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            mMap.setOnMarkerClickListener(this);
            mMap.setOnMapClickListener(this);
            if (mMap != null) {
                mMapPresenter.setUpMap(mMap);
            }
        }
    }


    private void centerMapOnMyLocation() {

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getBestProvider( new Criteria(), true);
        Location location = locationManager.getLastKnownLocation(provider);

        if(location!=null){
            onLocationChanged(location);
        }
        locationManager.requestLocationUpdates(provider, LOCATION_UPDATE_FREQ, 0, this);

    }

    public Context getContext(){
        return getApplicationContext();
    }

    @Override
    public void onLocationChanged(Location location) {
        mMapPresenter.updateMyLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void onVehiclesBtnClick(View view){
        mMapPresenter.toggleVehicles((FloatingActionButton) view);
    }

    public void onLocationBtnClick(View view){
        mMapPresenter.toggleLocation((FloatingActionButton) view);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mMapPresenter.toggleTooltip(marker, mTooltipLayout, mTooltipImage, mTooltipText);
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mTooltipLayout.animate().translationY(BitmapUtil.dipToPixels(getContext(), -64.0f));
    }
}
